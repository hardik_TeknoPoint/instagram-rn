import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import React, {Component} from 'react';
import Dp from '../components/dp';
import Icons from './Icons';
import PostMenu from './postMenu';

export default class PostHeader extends Component {
  state = {
    showMenu: false,
  };

  handleShowMenuBtn = () => {
    // console.log('showMenu===---', this.state.showMenu);
    this.setState({showMenu: !this.state.showMenu});
  };
  render() {
    return (
      <View style={styles.postHeaderImg}>
        <Dp width={40} height={40} />
        <Text style={styles.userName}>PostHeader</Text>
        <TouchableOpacity onPress={this.handleShowMenuBtn}>
          <Icons
            iconGrp={'Entypo'}
            iconName={'dots-three-vertical'}
            iconSize={20}

            //   iconColor={'black'}
          />
        </TouchableOpacity>
        {this.state.showMenu && (
          <PostMenu handleShowMenuBtn={this.handleShowMenuBtn} />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  postHeaderImg: {
    // position: 'relative',
    flexDirection: 'row',
    padding: 8,
    paddingHorizontal: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
    // backgroundColor: 'red',
  },
  userName: {
    flexGrow: 1,
    padding: 10,
    fontSize: 16,
    color: 'black',
    fontWeight: '600',
  },
});
