import {FlatList, StyleSheet, Text, View} from 'react-native';
import React, {Component} from 'react';
import Header from '../components/Header';
import ProfileDp from '../components/profileDp';
import Post from '../components/post';
import Footer from '../components/footer';

export default class HomeScreen extends Component {
  postData = [{}, {}, {}, {}, {}, {}];

  renderPost = ({item, index}) => {
    if (index === 0) return <ProfileDp />;
    return <Post />;
  };

  render() {
    return (
      <View style={styles.homeScreen}>
        <Header />

        <FlatList
          data={this.postData}
          renderItem={this.renderPost}
          style={styles.mainScroll}
        />
        <Footer />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  homeScreen: {
    // backgroundColor: 'red',
    // height: '100%',
    flex: 1,
  },
  mainScroll: {
    // width: 300,
    // flexBasis: 1,
    height: '70%',
    // backgroundColor: 'red',
  },
});
