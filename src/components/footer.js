import {StyleSheet, Text, View} from 'react-native';
import React, {Component} from 'react';
import Icons from '../reuseAbleComponents/Icons';
import Dp from './dp';

export default class Footer extends Component {
  render() {
    return (
      <View style={styles.footerNav}>
        {/* <Text>Footer</Text> */}
        <Icons
          iconGrp={'Entypo'}
          iconName={'home'}
          iconSize={28}
          iconStyle={''}
          iconColor={'#000'}
        />
        <Icons
          iconGrp={'AntDesign'}
          iconName={'search1'}
          iconSize={28}
          iconStyle={''}
          iconColor={'#000'}
        />
        <Icons
          iconGrp={'MaterialIcons'}
          iconName={'live-tv'}
          iconSize={28}
          iconStyle={''}
          iconColor={'#000'}
        />
        <Icons
          iconGrp={'AntDesign'}
          iconName={'hearto'}
          iconSize={28}
          iconStyle={''}
          iconColor={'#000'}
        />
        <Dp width={30} height={30} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  footerNav: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    // flexGrow: 1,
    width: '100%',
    height: 60,
    position: 'absolute',
    paddingHorizontal: 20,
    bottom: 0,
  },
});
