import {Text, View, Image, TouchableOpacity, StyleSheet} from 'react-native';
import React, {Component} from 'react';

export default class Dp extends Component {
  // isPost = this.props.isPost;
  render() {
    return (
      <TouchableOpacity
        style={[
          styles.dpBorder,
          {width: this.props.width, height: this.props.height},
        ]}>
        {this.props.img}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  dpBorder: {
    width: 70,
    height: 70,
    borderRadius: 70,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 3,
    borderColor: 'red',
  },
  dpImg: {
    width: '100%',
    height: '100%',
  },
});
