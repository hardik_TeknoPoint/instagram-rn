import {Text, View, Modal, StyleSheet} from 'react-native';
import React, {Component} from 'react';

export default class MyModel extends Component {
  state = {
    showModel: true,
  };
  render() {
    return (
      // <View>
      <Modal visible={this.state.showModel} style={styles.modelStyles}>
        <View style={styles.modelViewStyles}>
          <Text style={{fontSize: 20, fontWeight: '600'}}>
            Showing_Data.....
          </Text>
          <Text style={{fontSize: 20, fontWeight: '600'}}>
            Showing_Data.....
          </Text>
          <Text style={{fontSize: 20, fontWeight: '600'}}>
            Showing_Data.....
          </Text>
          <Text style={{fontSize: 20, fontWeight: '600'}}>
            Showing_Data.....
          </Text>
          <Text style={{fontSize: 20, fontWeight: '600'}}>
            Showing_Data.....
          </Text>
        </View>
      </Modal>
      // </View>
    );
  }
}

const styles = StyleSheet.create({
  modelStyles: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  modelViewStyles: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'green',
    width: '100%',
    height: '100%',
  },
});
